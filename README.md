### Introduction/Outline

Probe is a lightweight TCP/IP suite which tries to adhere two basic principles. Simplicity
and speed.

Usual protocol suite design tends to segregate each protocol differently and are handled
as different processes and communication happens through these processes.
While, Probe segregates each  'handling of packet' to run as a separate process for better
speed, as it reduces context switching.

Basically this is just a code design thinking which tries to follow an
object orientation into the stack structure as well.

# Build Requirements

The following packages are needed to build the software

* autotools (autoconf, autogen, automake, autopoint, libtool)

# Building the project

Download project and prepare source with:
	
		git clone https://github.com/Aniketh01/probe.git
		cd probe

if autotools is not installed in your system:

		sudo apt install automake    
		sudo apt install automake1.11
		
After the installation follow the below: 

		aclocal
		autoconf 
		autoheader
		automake --add-missing --foreign 

Build probe: 
	
		./configure 
		make 

Install probe: 
	
		sudo make install

**Please note**: Few of the naming resolutions and the Probe project architecture has
been inspired by the architecture followed by Wget2.

