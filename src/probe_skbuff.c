/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <probe.h>

#include <config.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <errno.h>

probe_skbuff_t *probe_skbuff_init(unsigned size)
{
	probe_skbuff_t *skb = malloc(sizeof(probe_skbuff_t));

	memset(skb, 0, sizeof(probe_skbuff_t));
	skb->data = malloc(size);
	memset(skb->data, 0, size);

	skb->refcount = 0;
	skb->head = skb->data;
	skb->end = skb->data + size;

	probe_list_init(&skb->list);

	return skb;
}

void probe_skbuff_deinit(probe_skbuff_t *skb)
{
	if (skb->refcount < 0) {
		xfree(skb->head);
		xfree(skb);
	}
}

uint8_t *probe_skbuff_push(probe_skbuff_t *skb, unsigned int len)
{
	skb->data -= len;
	skb->len +=len;

	return skb->data;
}

void *probe_skbuff_reserve(probe_skbuff_t *skb, unsigned int len)
{
	skb->data += len;

	return skb->data;
}

void probe_skbuff_reset_head(probe_skbuff_t *skb)
{
	skb->data = skb->end - skb->dlen;
	skb->len = skb->dlen;
}

uint8_t *probe_skbuff_get_head(probe_skbuff_t *skb)
{
	return skb->head;
}

probe_skbuff_t *probe_skbuff_peek(probe_skbuff_head_t *head)
{
	if(probe_skbuff_queue_check_empty(head)) return NULL;

	return probe_list_first_entry(&head->list, probe_skbuff_t, head);
}


/**
 * param[in] head A skb_head context
 *
 * Initialises a sk_buff queue with a skb linked list inside.
 */
void probe_skbuff_queue_init(probe_skbuff_head_t *head)
{
	probe_list_init(&head->list);
	head->qlen = 0;
}

/**
 * param[in] head A skb_head context
 *
 * The first entry of the queue is dequeued.
 *
 * \return the dequeued item.
 *
 */
probe_skbuff_t *probe_skbuff_dequeue(probe_skbuff_head_t *head)
{
	probe_skbuff_t *skb = probe_list_first_entry(&head->list, probe_skbuff_t, head);
	probe_list_delete(&skb->list);

	head->qlen -= 1;

	return skb;
}

/**
 * \param[in] head A skb_head context
 *
 * Free's the sk_buff queue.
 */
void probe_skbuff_queue_deinit(probe_skbuff_head_t *head)
{
	probe_skbuff_t *skb = NULL;

	while((skb = probe_skbuff_peek(head)) != NULL) {
		probe_skbuff_dequeue(head);
		skb->refcount--;
		probe_skbuff_deinit(head);
	}
}

/**
 * head A skb_head context
 *
 * \return queue length. (head->qlen)
 */
uint32_t probe_skbuff_queue_get_len(const probe_skbuff_head_t *head)
{
	return head->qlen;
}

/**
 * head A skb_head context
 *
 * Check if the circular queue of skb is empty or not.
 *
 * Returns True(1) if the queue is empty. Else False(0)
 *
 */
int probe_skbuff_queue_check_empty(probe_skbuff_head_t *head)
{
	if(probe_skbuff_queue_get_len(head) < 1) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * \param[in] head A skb_head context
 * \param[in] new A skb_head context reffering a new skb_head allocation
 * \param[in[ next A skb_head context location after the new skb_head inserted
 *
 * Insert new Skb context into the queue at a desired position, probably
 * a position ifront of the next skb_head context.
 */
void probe_skbuff_queue_add(probe_skbuff_head_t *head, probe_skbuff_t *new, probe_skbuff_t *next)
{
	probe_list_add(&new->list, &next->list);
	head->qlen += 1;
}

/**
 * \param[in] head A skb_head context
 * \param[in] new A skb_head context referring a new skb_head allocation
 *
 * Since buffer queues are usually managed in a FIFO manner and buffers are removed from the
 * head of the list, they are added to the list with probe_skbuff_queue_add_end().
 * Since this is a circular queue, it adds the skb context infront of current head,
 * that is, at the end of the queue.
 */
void probe_skbuff_queue_add_end(probe_skbuff_head_t *head, probe_skbuff_t *new)
{
	probe_list_add(&new->list, &head->list);
	head->qlen += 1;
}
