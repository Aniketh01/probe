/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */

#include <probe.h>

#include <config.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <errno.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

/**
 * \param[in] sock A Socket context
 * \param[in] family Network IP family. Either AF_INET or  AF_INET6 (IPv4, IPv6)
 * \param[in] transport The transport protocol in use. Either SOCK_STREAM or
 * SOCK_DGRAM (TCP or UDP)
 *
 * By default the socket connects through TCP connection.
 *
 * Creates a Server socket that listens for the clients to talk to.
 *
 * \return The socket descriptor.
 */
int probe_socket_server_create(probe_socket_t *sock, int family, int transport)
{
	int socketfd = -1;

	printf("Creating the server socket\n");

	if ((socketfd = socket(sock->family, sock->sock_type, 0)) < 0) {
		printf("Error : Could not create socket: %s\n", strerror(errno));
	    exit(EXIT_FAILURE);
	}

	return socketfd;
}

/**
 *  \param[in] sock A socket context
 *  \param[in] sockfd The socket descriptor
 *
 *	binds the socket with the values set in by sockaddr_in
 *
 *	\returns The resulting value of the bind. If failed -1.
 */
int probe_socket_server_bind(probe_socket_t *sock, int sockfd)
{
	int res = -1;

	struct sockaddr_in sockaddr_s = {
		.sin_family = sock->family,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port = htons(sock->port)
	};

	res = bind(sockfd, (struct sockaddr *)&sockaddr_s, sizeof(sockaddr_s));

	return res;
}
