/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <config.h>

#include <argp.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>

#include "probe_options.h"

#include <probe.h>

const char *argp_program_bug_address = "<anikethgireesh@gmail.com>";

static char doc[] = "Probe is a lightweight TCP/IP suite which tries to adhere two basic principles. Simplicity and speed. This is a educational library tries to adhere learning by doing technique, asking people to implement the API's calls and learn about TCP/IP";

static char args_doc[] = "";

// default values for config options (if not 0 or NULL)
struct opt_config opt_config = {
//	.test_phrase = "Hello, World!",
//	.server = "server sends a message",
//	.client = "client sends a message",
	.inet4_only = 0,
	.inet6_only = 0,
	.tcp = 0,
	.udp = 0,
};

enum {
	OPT_TCP = 1000,
	OPT_UDP = 1001,
	OPT_IP4 = 1002,
	OPT_IP6 = 1003,
	OPT_CONN_NO = 1004,
};

static struct argp_option options[] = {
	// long name, short name, name of the argument or 0, argp option flags, help message
	{"inet4-only", OPT_IP4, 0, 0, "Use IPv4 connections only. (default: off)"},
	{"inet6-only", OPT_IP6, 0, 0, "Use IPv6 connections only. (default: off)"},
	{"tcp", OPT_TCP, 0, 0, "select the TCP transport layer"},
	{"udp", OPT_UDP, 0, 0, "select the UDP transport layer"},
	{"no-connection", OPT_CONN_NO, "No_connection", 0, "The number of connections that the server listens to"},
    {"test-phrase",  'y', "TEST_PHRASE", 0, "Set a test phrase to be used in test (default : \"Hello, World!\")"},
	{"server", 'S', "server_message", 0, "Enable server functions for the sockets and send message to client via server"},
	{"client", 'C', "client_message", 0, "Enable client functions for the sockets and send message to server via client"},
	{0}
};

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	struct opt_config *cfg = state->input;
	
	switch(key) {
		case OPT_IP4:
			cfg->inet4_only += 1;
			break;
		case OPT_IP6:
			cfg->inet6_only += 1;
			break;
		case OPT_TCP:
			cfg->tcp += 1;
			break;
		case OPT_UDP:
			cfg->udp += 1;
			break;
		case OPT_CONN_NO:
			cfg->conn_no += 1;
			break;
        case 'y':
			cfg->test_phrase = strdup(arg);
			break;
		case 'S':
			cfg->server = strdup(arg);
			break;
		case 'C':
			cfg->client = strdup(arg);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

int init(int argc, char **argv)
{
	argp_parse(&argp, argc, argv, 0, 0, &opt_config);

	if(opt_config.inet6_only) {
		probe_socket_set_config_int(NULL, PROBE_SOCKET_ADDR_FAMILY, AF_INET6);
	}

	if(opt_config.inet4_only) {
		probe_socket_set_config_int(NULL, PROBE_SOCKET_ADDR_FAMILY, AF_INET);
	}

	if(opt_config.tcp) {
		probe_socket_set_config_int(NULL, PROBE_SOCKET_TYPE, SOCK_STREAM);
	}

	if(opt_config.udp) {
		probe_socket_set_config_int(NULL, PROBE_SOCKET_TYPE, SOCK_DGRAM);
	}

	if(opt_config.conn_no) {
		probe_socket_set_config_int(NULL, PROBE_SOCKET_CONN_NO, opt_config.conn_no);
	}

	if(opt_config.server) {
		int rc = probe_socket_server(NULL, opt_config.server, opt_config.conn_no);
	}

	if(opt_config.client) {
		int rc = probe_socket_client(NULL, opt_config.client);
	}
}

void deinit(void)
{
	xfree(opt_config.test_phrase);
	xfree(opt_config.server);
	xfree(opt_config.client);
}
