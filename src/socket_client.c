/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */

#include <config.h>

#include <probe.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/**
 * \param[in] sock A Socket context
 * \param[in] family Network IP family. Either AF_INET or  AF_INET6 (IPv4, IPv6)
 * \param[in] transport The transport protocol in use. Either SOCK_STREAM or
 * SOCK_DGRAM (TCP or UDP)
 *
 * By default the socket connects through TCP connection.
 *
 * Creates a client socket that talks with the server.
 *
 * \return The socket descriptor.
 */
int probe_socket_client_create(probe_socket_t *sock, int family, int transport)
{
	int socketfd = -1;

	printf("Creating the client socket\n");

	if ((socketfd = socket(sock->family, sock->sock_type, 0)) < 0) {
		printf("Error : Could not create socket: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	return socketfd;
}

/**
 * \param[in] sock A socket context
 * \param[in] sockfd The socket descriptor
 *
 * Connects to the socket with the values set in by sockaddr_in
 *
 * \returns The resulting value of the connect system call. If failed -1.
 */
int probe_socket_client_connect(probe_socket_t *sock, int sockfd)
{
	int res = -1;

	struct sockaddr_in sockaddr_c = {
		.sin_family = sock->family,
		.sin_addr.s_addr = inet_addr(sock->client_host),
		.sin_port = htons(sock->port)
	};
	
	res = connect(sockfd, (struct sockaddr *)&sockaddr_c, sizeof(sockaddr_c));

	return res;
}

/**
 * \param[in] sock A socket context
 * \param[in] sockfd The socket descriptor
 * \param[in] req The request parameter to be send to the server
 *
 * Manipulate the options set for the socket and then sends it to the server.
 *
 * \return The resulting value after the send system call. If failed -1.
 */
int probe_socket_client_send(probe_socket_t *sock, int sockfd, char *req)
{
	int res = -1;

	// Set the sender timeout to 10 seconds. 
	struct timeval timeout = {
		.tv_sec = 10,
		.tv_usec = 0
	};

	if(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
	  printf("The client timeout, Server isn't ready to listen: %s\n", strerror(errno));
	  exit(EXIT_FAILURE);
	}

	res = send(sockfd, req, strlen(req), 0);

	return res;
}

/**
 *  \param[in] sock A socket contex
 *  \param[in] sockfd The socket descriptor
 *  \param[in] resp The response from the server receiving the context
 *
 *  Manipulates the socket options and recieves the context response from the
 *  server.
 *
 *  \result The resulting value after the send system call. If failed -1.
 */
int probe_socket_client_receive(probe_socket_t *sock, int sockfd, char *resp)
{
	int res = -1;

	//set the receiver timeout
	struct timeval timeout = {
		.tv_sec = 10,
		.tv_usec = 0
	};

	if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
		printf("The client timeout: Server didn't respond back: %s\n", strerror(errno));
	    exit(EXIT_FAILURE);

	}

	res = recv(sockfd, resp, 1024, 0);

	printf("The response send %s\n", resp);

	return res;
}
