/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <probe.h>

/**
 * \param[in] head A probe_list/linked list context
 *
 * Initialises a new list (A Linked list implementation)
 *
 */
void probe_list_init(probe_list_t *head)
{
	head->prev = head->next = head;
}

/**
 * \param[in] new A probe list context to refer new node to be added
 * \param[in] head A probe list context
 *
 * Take the value for the new node and adds that to the linklist chain.
 *
 */
void probe_list_add(probe_list_t *new, probe_list_t *head)
{
	head->next->prev = new;
	new->next = head->next;
	new->prev = head;
	head->next = new;
}

/**
 * \param[in] ctx A probe list context
 *
 * Delete's a probe list node according to the node context passed.
 *
 */
void probe_list_delete(probe_list_t *ctx)
{
	probe_list_t *prev = ctx->prev;
	probe_list_t *next = ctx->next;

	prev->next = next;
	next->prev = prev;
}

/**
 * \param[in] new A probe list context to refer new node that need to be added
 * \param[in] head A probe list context
 *
 * Take the value for the new node and attaches that to the end of the linklist
 * chain.
 *
 */
void probe_list_add_end(probe_list_t *new, probe_list_t *head)
{
	head->prev->next = new;
	new->prev = head->prev;
	new->next = head;
	head->prev = new;
}

/**
 * \param[in] head A probe list context
 *
 * checks whether the list is empty.
 *
 * Returns True if the list is empty.
 */
void probe_list_check_empty(probe_list_t *head)
{
	return head->next == head;
}
