/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <linux/if_tun.h>

#include <probe.h>

#include <config.h>

#include <errno.h>

static probe_tap_t _global_tap = {
	.tap_addr = "10.0.0.5",
	.tap_route = "10.0.0.0/24"
};

/**
 * TAP kernel virtualisation interface, referred from:
 * https://www.kernel.org/doc/Documentation/networking/tuntap.txt
 *
 * char *dev should be the name of the device with a format string (e.g."tun%d"), 
 * but (as far as I can see) this can be any valid network device name.
 *
 * Note that the character pointer becomes overwritten with the real device
 * name (e.g. "tun0")
 * 
 * Documentation:
 *
 * TUN/TAP provides packet reception and transmission for user space programs.
 * It can be seen as a simple Point-to-Point or Ethernet device, which,
 * instead of receiving packets from physical media, receives them from
 * user space program and instead of sending packets via physical media
 * writes them to the user space program.
 */
int tun_alloc(char *dev)
{
	struct ifreq ifr;
	int fd, err;

	if(!dev)
		return -1;
	
	if((fd = open("/dev/net/tap", O_RDWR)) < 0)
		printf("Probe error: Cannot open TUN/TAP dev %s\n", strerror(errno));
		exit(EXIT_FAILURE);

	memset(&ifr, 0, sizeof(ifr));

	/* Flags: IFF_TUN   - TUN device (no Ethernet headers)
	 *		  IFF_TAP   - TAP device
	 *
	 *		  IFF_NO_PI - Do not provide packet information 
	 */

	ifr.ifr_flags = IFF_TAP | IFF_NO_PI;

	if(*dev)
		strncpy(ifr.ifr_name, dev, IFNAMSIZ);

	if((err=ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0) {
		printf("Probe error: Couldn't %s\n", strerror(errno));
		close(fd);
		return err;
	}

	strcpy(dev, ifr.ifr_name);
	return fd;
}

/**
 * \return A new TAP device context
 *
 * Assigns the default values to the initial TAP device interface context.
 * Further, allocates memory for the device and initialises and allocates the
 * TAP interface.
 *
 */
probe_tap_t* probe_tap_init(void)
{
	probe_tap_t *tap = malloc(sizeof(probe_tap_t));
	size_t len = 10;

	*tap = _global_tap;

	tap->dev =  (char*) calloc(len, sizeof(char));
	tap->tap_fd = tun_alloc(tap->dev);

	return tap;
}

/**
 * \param[in] tap A TAP device context
 *
 * The `tap device` will be freed.
 */
void probe_tap_deinit(probe_tap_t* tap)
{
	xfree(tap->dev);
}

/**
 * \param[in] tap A TAP device context
 * \param[in] buf buffer to receive content from TAP device
 * \param[in] len Length of the buffer
 *
 * Read the context send over from the network device driver
 * through the TAP device interface.
 */
int probe_tap_read(probe_tap_t *tap, char *buf, int len)
{
	int rc;

	if((rc = read(tap->tap_fd, buf, len)) < 0) {
		printf("Probe_TAP error: Failed to read %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	return rc;
}

/**
 * \param[in] tap A TAP device context
 * \param[in] buf buffer to send to TAP device
 * \param[in] len Length of the buffer
 *
 * Write the context from the stack to the TAP device inorder
 * to communicate the network device driver.
 */
int probe_tap_write(probe_tap_t *tap, char *buf, int len)
{
	int rc;

	if((rc = write(tap->tap_fd, buf, len)) < 0) {
		printf("Probe_TAP error: Failed to write %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	return rc;
}
