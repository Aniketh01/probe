/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <probe.h>

#include <config.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <errno.h>

#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>

#define LENGTH 1024

static probe_socket_t _global_sock = {
	.port = 8080,
	.family = AF_INET,
	.conn_no = 3,
	.sock_type = SOCK_STREAM,
	.client_host = "127.0.0.1"
};

/**
 * \return A new socket context
 *
 * Assigns the  default values to the initial socket context.
 *
 */
probe_socket_t* probe_socket_init(void)
{
	probe_socket_t *sock = malloc(sizeof(probe_socket_t));

	*sock = _global_sock;
	return sock;
}

/**
 * \param[in] sock A socket context
 *
 * The `probe_socket_t` structure will be freed.
 */
void probe_socket_deinit(probe_socket_t* sock)
{
	xfree(sock);
}

/**
 * \param[in] sock A socket context
 * \param[in] key An config parameter identifier
 * \param[in] value The value to be assigned to the config
 *
 * Set a configuration parameter as an integer.
 *
 * The available config parameters are:
 *	- PROBE_SOCKET_TYPE: Set which type of communication exits between the
 *	sockets of client and the server. That is, it sets to TCP or UDP according
 *	to whether the caller needs it or not.
 *	- PROBE_SOCKET_ADDR_FAMILY: sets the preferred address family. This will
 *	typically be `AF_INET` or `AF_INET6`, but it can be any of the
 *	values defined in `<socket.h>`. Additionally, `AF_UNSPEC` means you don't care.
 *	- PROBE_SOCKET_PORT: sets the port for the caller to allow the server to
 *	talk with or the client to listen to.
 *	- PROBE_SOCKET_CONN_NO: set the number of connections that the socket can
 *	wait for. Most of the systems only supports max of 5.
 */
void probe_socket_set_config_int(probe_socket_t* sock, int key, int value)
{
	if(!sock)
		sock = &_global_sock;

	switch(key) {
		case PROBE_SOCKET_ADDR_FAMILY:
			sock->family = value;
			break;
		case PROBE_SOCKET_TYPE:
			sock->sock_type = value;
			break;
		case PROBE_SOCKET_PORT:
			sock->port = value;
			break;
		case PROBE_SOCKET_CONN_NO:
			sock->conn_no = value;
			break;
		default:
			printf("Unknown config key %d\n", key);
			break;
	}
}

/**
 *  \param[in] A socket context
 *  \param[in] key An identifier to the config parameter.
 *  \param[in] value the value to be set for the config parameter. 
 *
 *  Currently available parameter is to set PROBE_CLIENT_HOST_ADDR (default:
 *  localhost), which sets the target host IP of the server address the client
 *  wish to communicate with. 
 */
void probe_socket_set_config_string(probe_socket_t *sock, int key, const char *value)
{
	if (key == PROBE_CLIENT_HOST_ADDR)
		sock->client_host = value;
	else
		printf("Unknown config key %d\n", key);
}

/**
 *  \param[in] sock A socket context
 *
 *  The driver function for the server socket. 
 */
int probe_socket_server(probe_socket_t *sock, const char *msgbuf, int conn)
{
	if(!sock)
		sock = &_global_sock;

	int sockfd, sock_accept, connfd, bind, read_size;
	struct sockaddr_in client;
	char recvBuff[200] = {0};
	char message[200] = {0};

	const char *pmsg = "Probe";

	memset(&client, '0', sizeof(client));

	int client_size = sizeof(struct sockaddr_in);

	if((sockfd = probe_socket_server_create(sock, PROBE_NET_FAMILY_IPv4, sock->sock_type)) < 0) {
		printf("Probe error: Couldn't create the server socket: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	if((bind = probe_socket_server_bind(sock, sockfd)) < 0) {
	   printf("Probe error: Couldn't bind to the server: %s\n", strerror(errno));
	   exit(EXIT_FAILURE);
	}

	listen(sockfd, sock->conn_no);

	while(1) {
		printf("accepting incoming messages\n");

		if((sock_accept = accept(sockfd, (struct sockaddr *)&client, (socklen_t*)&client_size)) < 0) {
			printf("Probe: Accept failed: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}

		printf("Connection established\n");
	
		memset(recvBuff, '\0', sizeof(recvBuff));
		memset(message, '\0', sizeof(message));

		if(recv(sock_accept, recvBuff, 200, 0) < 0) {
			printf("Probe: Recieve failed: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}

		printf("client reply:%s\n", recvBuff);

		if(strcmp(pmsg, recvBuff) == 0)
			strcpy(message, "Hello from probe");
		else
			strcpy(message, "Invalid message");

		if(send(sock_accept, message, strlen(message), 0) < 0) {
			printf("Probe error: send failed: %s\n", strerror(errno));
		    exit(EXIT_FAILURE);
		}

		close(sock_accept);
		sleep(1);
	}

	return 0;
}

/**
 * \param[in] sock A socket context
 *
 * The driver function for the client socket. 
 */
int probe_socket_client(probe_socket_t *sock, const char *msgbuf)
{
	if(!sock)
		sock = &_global_sock;

	int sockfd, read_size, connect, send;
	struct sockaddr_in server;
	char sendBuff[100] = {0};
	char SendToServer[100] = {0};

	memset(&server, '0', sizeof(server));

	if((sockfd = probe_socket_client_create(sock, PROBE_NET_FAMILY_IPv4, sock->sock_type)) < 0) {
         printf("Probe error: Couldn't create the client socket: %s\n", strerror(errno));
		 exit(EXIT_FAILURE);
	}

	if((connect = probe_socket_client_connect(sock, sockfd)) < 0) {
		printf("Probe error: Couldn't connect to the server: %s\n", strerror(errno));
	    exit(EXIT_FAILURE);
	}

	printf("Probe Success: Successfully connected to the server\n");

	printf("Enter the message to pass on to the server: ");
	
	fgets(SendToServer, sizeof(SendToServer), stdin);
	SendToServer[strcspn(SendToServer, "\n")] = 0;

	if((probe_socket_client_send(sock, sockfd, SendToServer)) < 0) {
		printf("Probe error: The client couldn't send: %s\n", strerror(errno));
	    exit(EXIT_FAILURE);
	}

	read_size = probe_socket_client_receive(sock, sockfd, sendBuff);

	printf("Reply from the probe server: %s\n", sendBuff);

	close(sockfd);

	for (int i = 1; i <= sock->conn_no; i++)
		shutdown(sockfd, i);

	return 0;
}
