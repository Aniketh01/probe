/*
 * MIT License
 *
 * Copyright (c) 2018 Aniketh Girish
 * This file is part of libprobe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PROBE_PROBE_H
#define PROBE_PROBE_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include <unistd.h>

#ifdef PROBEVER_FILE
#	include PROBEVER_FILE
#else
#	include "probever.h"
#endif


#define xfree(a) do { if (a) { free((void *)(a)); a=NULL; } } while (0)

/**
 * List (Linked list) routines
 */

struct probe_list_st {
	struct probe_list_st
		*next,
		*prev;
};

typedef struct probe_list_st probe_list_t;

#define probe_list_head(name) struct probe_list name = { &(name), &(name) }
#define probe_list_entry(ptr, type, member) ((type *) ((char *) (ptr) - offsetof(type, member)))
#define probe_list_first_entry(ptr, type, member) probe_list_entry((ptr)->next, type, member)
#define probe_list_each(pos, head) for (pos = (head)->next; pos != (head); pos = pos->next)
#define probe_list_for_each_safe(pos, p, head) for (pos = (head)->next, p = pos->next; pos != (head); pos = p, p = pos->next)

void
	probe_list_init(probe_list_t *head);
void
	probe_list_add(probe_list_t *new, probe_list_t *head);
void
	probe_list_delete(probe_list_t *);
void
	probe_list_add_end(probe_list_t *new, probe_list_t *head);
void
	probe_list_check_empty(probe_list_t *head);

/**
 * sk_buff routines
 */

struct probe_skbuff_st {
	probe_list_t list;
	uint8_t
		*head,
		*end,
		*data,
		*tail,
		*payload;
	uint16_t
		protocol;
	uint32_t
		len,
		dlen,
		seq,
		end_seq;
	int
		refcount;
};

typedef struct probe_skbuff_st probe_skbuff_t;

struct probe_skbuff_head_st {
	probe_list_t list;
	uint32_t qlen; // Number of buffers in the list
};

typedef struct probe_skbuff_head_st probe_skbuff_head_t;

probe_skbuff_t
	*probe_skbuff_init(unsigned int size);
probe_skbuff_t
	*probe_skbuff_peek(probe_skbuff_head_t *head);
void
	probe_skbuff_deinit(probe_skbuff_t *skb);
uint8_t
	*probe_skbuff_push(probe_skbuff_t *skb, unsigned int len);
void
	*probe_skbuff_reserve(probe_skbuff_t *skb, unsigned int len);
void
	probe_skbuff_reset_head(probe_skbuff_t* skb);
uint8_t
	*probe_skbuff_get_head(probe_skbuff_t *skb);

// Probe sk buff datagram/head queue function design

void
	probe_skbuff_queue_init(probe_skbuff_head_t *head);
void
	probe_skbuff_queue_deinit(probe_skbuff_head_t *head);
probe_skbuff_t
	*probe_skbuff_dequeue(probe_skbuff_head_t *head);
void
	probe_skbuff_queue_add(probe_skbuff_head_t *head, probe_skbuff_t *new, probe_skbuff_t *next);
void
	probe_skbuff_queue_add_end(probe_skbuff_head_t *head, probe_skbuff_t *new);
int
	probe_skbuff_queue_check_empty(probe_skbuff_head_t *head);
uint32_t
	probe_skbuff_queue_get_len(const probe_skbuff_head_t *head);

/**
 * Net family routines
 */

#define PROBE_NET_FAMILY_IPv4 1
#define PROBE_NET_FAMILY_IPv6 2

/**
 * * Transport macros
 */

#define PROBE_TRANSPORT_PROTOCOL_UDP 1
#define PROBE_TRANSPORT_PROTOCOL_TCP 2

/*
 * Socket Server/Client routines
 */

#define PROBE_SOCKET_ADDR_FAMILY 1
#define PROBE_SOCKET_TYPE 2
#define PROBE_SOCKET_PORT 3
#define PROBE_SOCKET_CONN_NO 4	// Most of the system only support max of 5.

#define PROBE_CLIENT_HOST_ADDR 1

struct probe_socket_st {
	int
		family,
		port,
		conn_no,
		sock_type;
	const char
		*client_host;
};

typedef struct probe_socket_st probe_socket_t;

probe_socket_t*
	probe_socket_init(void);

void 
	probe_socket_deinit(probe_socket_t*);
void 
	probe_socket_set_config_int(probe_socket_t *, int key, int value);
void
	wget_dns_set_config_string(probe_socket_t *, int key, const char *value);
int 
	probe_socket_server_create(probe_socket_t *, int family, int transport);
int 
	probe_socket_server_bind(probe_socket_t *, int sockfd);
int
	probe_socket_client_create(probe_socket_t *, int family, int transport);
int
	probe_socket_client_connect(probe_socket_t *, int sockfd);
int 
	probe_socket_client_send(probe_socket_t *, int sockfd, char* req);
int 
	probe_socket_client_receive(probe_socket_t *, int sockfd, char* resp);	
int
	probe_socket_client(probe_socket_t *, const char *msgbuf);
int
	probe_socket_server(probe_socket_t *, const char *msgbuf, int conn);

/*
 * TUN/TAP kernel interface routines
 */

struct probe_tap_st {
	int tap_fd;
	char
		*dev;
	const char
		*tap_addr,
		*tap_route;
};

typedef struct probe_tap_st probe_tap_t;

probe_tap_t*
	probe_tap_init(void);
void
	probe_tap_deinit(probe_tap_t *);
int
	probe_tap_read(probe_tap_t *, char *buf, int len);
int
	probe_tap_write(probe_tap_t *, char *buf, int len);

/*
 * Ethernet frame handling routines
 */

#define ETHR_SIZE	6	/* Size of Ethernet address */

struct probe_ethr_frame_st {
	uint8_t dest_addr[ETHR_SIZE];
	uint8_t src_addr[ETHR_SIZE];
	uint16_t ethr_type;
	uint8_t  *payload;
} __attribute__((packed));

typedef struct probe_ethr_frame_st probe_ethr_frame_t;

probe_ethr_frame_t*
	probe_ethr_frame_init(struct sk_buff *skb);

#endif /* PROBE_PROBE_H */
