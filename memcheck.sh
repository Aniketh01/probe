#!/bin/bash
# script for memory leak test
# -v verbose model
valgrind -q --leak-check=full --show-reachable=yes ./probe

